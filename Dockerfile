FROM openjdk:8
EXPOSE 9092
ADD target/restdocker.jar RestDocker.jar
ENTRYPOINT ["java", "-jar", "/RestDocker.jar"]